const chalk = require('chalk')
const load = require('./loadAndSave.js')

const addNote = (title,body) => {
    const notes = load.loadNotes()
    // const duplicateNotes = notes.filter((note) => note.title === title) 
    const duplicateNote = notes.find((note) => note.title === title)
    //checking for duplicates 
    if (!duplicateNote){
        notes.push({
            title: title,
            body: body
        })
        load.savedNotes(notes)
        console.log('New note added!')
    } else {
        console.log('Note title taken!')
    }
    
}

const removeNote = (title) => {
    const notes = load.loadNotes()
    const notesToKeep = notes.filter((note)=> note.title !== title )
    console.log(chalk.inverse.red('Note has been removed!'))
    
    load.savedNotes(notesToKeep)   
}

module.exports = {
    removeNote: removeNote,
    addNote: addNote
}