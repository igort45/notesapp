const fs = require('fs')


const savedNotes = function(notes){
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json',dataJSON)
}


const loadNotes = function(){
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJson = dataBuffer.toString()
        return JSON.parse(dataJson)
    } catch(e){
        return []
    }
    
}

module.exports =  {
    savedNotes: savedNotes,
    loadNotes: loadNotes
}