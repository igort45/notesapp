const chalk = require('chalk')
const load = require('./loadAndSave.js')

const listNotes = () => {
    const notes = load.loadNotes()
    console.log(chalk.inverse('Your notes'))
    notes.forEach(note => {
        console.log(note.title)
    });
}

const readNotes = (title) => {
    const notes = load.loadNotes()
    const note = notes.find((note)=> note.title === title)
    if (note) {
        console.log(chalk.inverse(note.title))
        console.log(note.body)
    } else {
        console.log(chalk.red('Note not found!'))

    }
}

module.exports = {
    listNotes: listNotes,
    readNotes: readNotes
}

