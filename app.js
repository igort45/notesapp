const chalk = require('chalk')
// const notes = require('./notes.js')
const addRemove = require('./addAndRemove.js')
const listRead = require('./listandRead.js')
const yargs = require('yargs')

yargs.version('1.1.0')

yargs.command({
    command: 'add',
    describe: 'Adding a new note',
    builder: {
        title: {
            describe: 'Note Title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note Body',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        addRemove.addNote(argv.title,argv.body)        
    }
})

yargs.command({
    command: 'remove',
    describe: 'Removing a note',
    builder: {
        title: {
            describe: 'Note Title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        addRemove.removeNote(argv.title)
    }
})

yargs.command({
    command: 'list',
    describe: 'Listing notes',
    handler() {
        listRead.listNotes()
    }
})

yargs.command({
    command: 'read',
    describe: 'Reading a note',
    builder: {
        title: {
            describe: 'Reading a note',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function(argv){
        listRead.readNotes(argv.title)
    }
})

yargs.parse()
